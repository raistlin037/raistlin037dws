<?php
include_once('Config.php');
include_once("funciones.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>

        <?php cabecera(); ?>

        <h3>Resultado del alta</h3>
        <h4>Autor Alumno 3: Javier Alcolea Ruiz</h4>

        <?php
        $id = recoge("id");
        if ($id == "")
          $id = "Error: Vacio";

        $nombre = recoge("nombre");
        if ($nombre == "")
          $nombre = "Error: Vacio";
        ?>

        <table border="1">
            <tr>
                <td>Id</td>
                <td>
                    <?php echo $id; ?>
                </td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td><?php echo $nombre; ?></td>
            </tr>

        </table>

        <p><a href="MenuTU.php">Atras</a> </p>
        <?php pie(); ?>

    </body>
</html>
