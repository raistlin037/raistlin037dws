<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

        function leer() {

            $id = recoge("id");
            $nombre = recoge("nombre");
            $url = recoge("url");
            $tipoenlace = recoge("tipoenlace");


            $enlace[0] = $id;
            $enlace[1] = $nombre;
            $enlace[2] = $url;
            $enlace[3] = $tipoenlace;
            return $enlace;

        }

        //***************************
        //* Main
        //***************************

        $enlace = leer();

        if ($enlace[0] != "" && $enlace[1] != "") {

            grabarEnlace($enlace);
            echo "Grabado enlace. ";
            echo '<a href="EnlacesMenu.php">Seguir</a>';
            //echo "Grabado: " . $enlace->getNombre() . "<br>";
        } else {
            //echo "Error: Campos vacios" . "<br>";

        }

        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
