<?php
include_once('Config.php');
include_once("funciones.php");
include_once("Ficheros.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>

        <p>Gestión de Enlaces:</p>
        <ul>
            <li><a href="EnlaceFormulario.php">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id </td>';
        echo '<td>Nom </td>';
        echo '<td>Url </td>';
        echo '<td>TipoUrl</td>';
        echo '<td>Borrar</td>';
        echo '<td>Actualizar</td>';
        echo '</tr>';



        $enlaces = getEnlaces();

        if (count($enlaces) > 0) {

            foreach ($enlaces as $enlace) {
                echo "<tr>\n";
                echo "<td>" . $enlace[0] . "</td>\n";
                echo "<td>" . $enlace[1] . "</td>\n";
                echo "<td>" . $enlace[2] . "</td>\n";
                echo "<td>" . $enlace[3] . "</td>\n";
                echo '<td> <a href="EnlacesBorrar.php?id=' . $enlace[0] . '">Borrar </td>';
                echo '<td> <a href="EnlacesActualizar.php?id=' . $enlace[0] . '">Actualizar </td>';
                echo "</tr>\n";
            }

        }

        echo "</table>";
        echo "<br/>";


        volver();
        pie();
        ?>

</html>
