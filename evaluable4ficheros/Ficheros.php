<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

// Funciones Javier Alcolea
//**************************************
function getTipoUsuarios() {
    $tipoUsuarios = array();
    $tipoUsuario = array();

    $ftu = "TipoUsuarios.txt";
    $f = fopen($ftu, "r");
    if ($f) {
        $data = fgetcsv($f, 1000, ";");
        $cont = 0;
        $i = 0;
        while ($data) {
            $tipoUsuario[0] = $data[0];
            $tipoUsuario[1] = $data[1];
            $tipoUsuarios[$cont] = $tipoUsuario;
            $cont++;
            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);

        return $tipoUsuarios;
    }
}

//**************************************
function grabarTipoUsario($tipoUsuario) {

    $ftu = "TipoUsuarios.txt";
    $f = fopen($ftu, "a");
    $linea = $tipoUsuario[0]
      . ";" . $tipoUsuario[1]
      . "\r\n";
    fwrite($f, $linea);

    fclose($f);
}

function borrarTiposDeUsuario($tipoUsuario) {

    $lineas = file("TipoUsuarios.txt"); 
    foreach ($lineas as $linea) { //Recorremos el array
        $array = explode(";", $linea);
        if ($array[0] == $tipoUsuario) { //Si es igual que el elemento que pasamos como parámetro
            $pos = array_search($linea,$lineas); //Guardamos la posicion en el array
            unset($lineas[$pos]); //La borramos
        }
    }
    $ftu = "TipoUsuarios.txt";
    $f = fopen($ftu, "w"); 
    foreach ($lineas as $linea) { 
        fwrite($f, $linea);
    }
    fclose($f); 
}

//Funciones profesor
//**************************************
function getEnlaces() {
    $enlaces = array();
    $enlace = array();

    $fe = "Enlaces.txt";
    $f = fopen($fe, "r");
    if ($f) {
        $data = fgetcsv($f, 1000, ";");
        $cont = 0;
        $i = 0;
        while ($data) {
            $enlace[0] = $data[0];
            $enlace[1] = $data[1];
            $enlace[2] = $data[2];
            $enlace[3] = $data[3];
            $enlaces[$cont] = $enlace;
            $cont++;
            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);

        return $enlaces;
    }
}

//**************************************
function getEnlace($enlace_) {

    $fe = "Enlaces.txt";
    $f = fopen($fe, "r");
    $data = fgetcsv($f, 1000, ";");


    while ($data) {
        $idp = $data[0];
        //echo "Prof : " . $idp . " ==  " . $enlace_->getId() . "?<br>";
        if ($idp == $enlace_[0]) {
            $enlace[0] = $data[0];
            $enlace[1] = $data[1];
            $enlace[2] = $data[2];
            $enlace[3] = $data[3];
            break;
        }

        $data = fgetcsv($f, 1000, ";");
    }
    fclose($f);
    //echo "Leido Objeto: " . $enlace->getId() . " " . $enlace->getNombre() . "<br>";
    return $enlace;
}

//**************************************
function grabarEnlace($enlace) {

    $fe = "Enlaces.txt";
    $f = fopen($fe, "a");
    $linea = $enlace[0]
      . ";" . $enlace[1]
      . ";" . $enlace[2]
      . ";" . $enlace[3]
      . "\r\n";
    fwrite($f, $linea);

    fclose($f);
}
?>

