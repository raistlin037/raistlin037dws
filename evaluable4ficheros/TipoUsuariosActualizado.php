<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

            $id = recoge("id");
            $nombre = recoge("nombre");
            $pos = recoge("pos");

            $tipoUsuario[0] = $id;
            $tipoUsuario[1] = $nombre;

        //***************************
        //* Main
        //***************************


        if ($tipoUsuario[0] != "" && $tipoUsuario[1] != "" && $pos != "") {

            $lineas = file("TipoUsuarios.txt"); 
            $linea = $tipoUsuario[0] . ";" . $tipoUsuario[1] . "\r\n";
            $lineas[$pos] = $linea;
            $ftu = "TipoUsuarios.txt";
            $f = fopen($ftu, "w"); 
            foreach ($lineas as $linea) { 
                fwrite($f, $linea);
            }
            fclose($f);
        } else {
            echo "Error: Campos vacios" . "<br>";

        }

        echo "Tipo de usuario actualizado. ";
        echo '<a href="TipoUsuariosMenu.php">Seguir</a>';
        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
