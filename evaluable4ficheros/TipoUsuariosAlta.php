<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

        function leer() {

            $id = recoge("id");
            $nombre = recoge("nombre");

            $tipoUsuario[0] = $id;
            $tipoUsuario[1] = $nombre;
            return $tipoUsuario;

        }

        //***************************
        //* Main
        //***************************

        $tipoUsuario = leer();

        if ($tipoUsuario[0] != "" && $tipoUsuario[1] != "") {

            grabarTipoUsario($tipoUsuario);
            echo "Grabado tipo de usuario. ";
            echo '<a href="TipoUsuariosMenu.php">Seguir</a>';
            //echo "Grabado: " . $enlace->getNombre() . "<br>";
        } else {
            echo "Error: Campos vacios" . "<br>";

        }

        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
