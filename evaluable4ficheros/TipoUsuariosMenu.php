<?php
include_once('Config.php');
include_once("funciones.php");
include_once("Ficheros.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>

        <p>Gestión de Tipos de Usuario:</p>
        <ul>
            <li><a href="TipoUsuariosFormulario.php">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id </td>';
        echo '<td>Nombre </td>';
        echo '<td>Borrar</td>';
        echo '<td>Actualizar</td>';
        echo '</tr>';



        $tipoUsuarios = getTipoUsuarios();

        if (count($tipoUsuarios) > 0) {

            foreach ($tipoUsuarios as $tipoUsuario) {
                echo "<tr>\n";
                echo "<td>" . $tipoUsuario[0] . "</td>\n";
                echo "<td>" . $tipoUsuario[1] . "</td>\n";
                echo '<td> <a href="TiposUsuarioBorrar.php?id=' . $tipoUsuario[0] . '">Borrar </td>';
                $pos = array_search($tipoUsuario,$tipoUsuarios); //Guardamos la posición
                echo '<td> <a href="TiposUsuarioActualizar.php?pos=' . $pos .'">Actualizar </td>';
                echo "</tr>\n";
            }

        }

        echo "</table>";
        echo "<br/>";


        volver();
        pie();
        ?>

</html>
