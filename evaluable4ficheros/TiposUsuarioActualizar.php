<?php
include_once('Config.php');
include_once("funciones.php");
?>

<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        ?>

        <?php cabecera(); ?>

        <?php
            function leer() {

                $pos = recoge("pos");
                return $pos;
            }
            $pos = leer();
            $lineas = file("TipoUsuarios.txt");
            $linea = explode(";", $lineas[$pos]);
        ?>

        <h3>Actualizar Tipo Usuario</h3>

        <form action="TipoUsuariosActualizado.php" method="post">
            <table>
                <tr>
                    <td>Id</td>
                    <td><input type="text" name="id" value=<?php echo $linea[0]; ?> /></td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td><input type="text" name="nombre" value=<?php echo $linea[1]; ?> /></td>
                </tr>
            </table>

            <table>
                <tr>
                    <td>
                        <input type="submit" value="Modificar" />
                    </td>
                    <td>
                        <input type="reset" value="Borrar" />
                    </td>
                </tr>
            </table>
            <input type="hidden" name="pos" value=<?php echo $pos; ?> />
        </form>



        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>