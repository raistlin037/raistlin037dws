<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Enlaces.php");
include_once("TiposDeUsuarios.php");

class Ficheros {

  private $fe = "Enlaces.txt";
  private $fa = "Tipoenlaces.txt";
  private $ftu = "TipoUsuarios.txt";

  //**************************************
  public function getEnlaces() {
    $enlaces = array();

    $f = @fopen($this->fe, "r");

    if ($f) {
      $data = fgetcsv($f, 1000, ";");
      $cont = 0;
      while ($data) {
        $enlace = new Enlaces($data[0], $data[1], $data[2], $data[3]);
        $enlaces[$cont] = $enlace;
        $cont++;
        $data = fgetcsv($f, 1000, ";");
      }
      fclose($f);

      //echo "Num prof " . count($enlaces) . "<br>";
      //print_r($enlaces);
    } else {
      //echo "Error: No se puede abrir: " . $this->fe . "<br>";
    }

    return $enlaces;
  }

  //**************************************
  function getEnlace($enlace_) {
    $f = fopen($this->fe, "r");
    $data = fgetcsv($f, 1000, ";");
    $enlace = new Enlaces("", "");

    while ($data) {
      $idp = $data[0];
      //echo "Prof : " . $idp . " ==  " . $enlace_->getId() . "?<br>";
      if ($idp == $enlace_->getId()) {
        $enlace->setId($data[0]);
        $enlace->setNombre($data[1]);
        $enlace->setUrl($data[2]);
        $enlace->setTipoenlace($data[3]);
        break;
      }

      $data = fgetcsv($f, 1000, ";");
    }
    fclose($f);
    //echo "Leido Objeto: " . $enlace->getId() . " " . $enlace->getNombre() . "<br>";
    return $enlace;
  }

  //**************************************
  function grabarEnlace($enlace) {


    $f = fopen($this->fe, "a");
    $linea = $enlace->getId()
            . ";" . $enlace->getNombre()
            . ";" . $enlace->getUrl()
            . ";" . $enlace->getTipoenlace()
            . "\r\n";
    fwrite($f, $linea);

    fclose($f);
  }

  //**************************************
  function borrarEnlace($enlace) {

    // Leo todos los enlaces en un vector
    $enlaces = array();
    $enlaces = getEnlaces();

    // Borro el fichero
    ulink($this->fe);

    // Grabo todos los enlace del vector.
    if (count($enlaces) > 0) {
      foreach ($enlaces as $enlace) {
        grabarEnlace($enlace);
      }
    }
  }

  //************************************** Javier Alcolea
  public function getTiposDeUsuarios() {
    $tiposUsuarios = array();

    $f = @fopen($this->ftu, "r");

    if ($f) {
      $data = fgetcsv($f, 1000, ";");
      $cont = 0;
      while ($data) {
        $tiposUsuario = new TiposDeUsuarios($data[0], $data[1]);
        $tiposUsuarios[$cont] = $tiposUsuario;
        $cont++;
        $data = fgetcsv($f, 1000, ";");
      }
      fclose($f);

    } else {
      //echo "Error: No se puede abrir: " . $this->ftu . "<br>";
    }

    return $tiposUsuarios;
  }

  //**************************************
  function getTiposDeUsuario($tiposUsuario_) {
    $f = fopen($this->ftu, "r");
    $data = fgetcsv($f, 1000, ";");
    $tiposUsuario = new TiposDeUsuarios("", "");

    while ($data) {
      $idp = $data[0];
      if ($idp == $tiposUsuario_->getId()) {
        $tiposUsuario->setId($data[0]);
        $tiposUsuario->setNombre($data[1]);
        break;
      }

      $data = fgetcsv($f, 1000, ";");
    }
    fclose($f);
    return $tiposUsuario;
  }

  //**************************************
  function grabarTiposDeUsuario($tiposUsuario) {


    $f = fopen($this->ftu, "a");
    $linea = $tiposUsuario->getId()
            . ";" . $tiposUsuario->getNombre()
            . "\r\n";
    fwrite($f, $linea);

    fclose($f);
  }

  //**************************************
  function borrarTiposDeUsuario($tipoUsuario_) {

    $tiposUsuarios = $this->getTiposDeUsuarios();

    foreach ($tiposUsuarios as $tiposUsuario) { //Recorremos el array de objetos
        if ($tiposUsuario->getId() == $tipoUsuario_) { //Si es igual que el elemento que pasamos como parámetro
            $pos = array_search($tiposUsuario,$tiposUsuarios); //Guardamos la posicion en el array
            unset($tiposUsuarios[$pos]); //La borramos
        }
    }

    unlink($this->ftu);

    foreach ($tiposUsuarios as $tiposUsuario) { 
      $this->grabarTiposDeUsuario($tiposUsuario);
    }
  }

}

?>