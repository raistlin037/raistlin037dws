<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

            $id = recoge("id");
            $nombre = recoge("nombre");
            $pos = recoge("pos");

        //***************************
        //* Main
        //***************************


        if ($id != "" && $nombre != "" && $pos != "") {

            $datos = new Ficheros(); 
            $tiposUsuarios = $datos->getTiposdeUsuarios();
            $tiposUsuarios[$pos]->setId($id);
            $tiposUsuarios[$pos]->setNombre($nombre);

            $ftu = "TipoUsuarios.txt";
            unlink($ftu);

            foreach ($tiposUsuarios as $tiposUsuario) { 
            $datos->grabarTiposDeUsuario($tiposUsuario);
            }

        } else {
            echo "Error: Campos vacios" . "<br>";
        }

        echo "Tipo de usuario actualizado. ";
        echo '<a href="TiposUsuariosMenu.php">Seguir</a>';
        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
