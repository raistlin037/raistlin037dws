<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

        function leer() {
          $id = recoge("id");
          $nombre = recoge("nombre");

          $tiposUsuario = new TiposDeUsuarios($id, $nombre);
          return $tiposUsuario;
        }

        //***************************
        //* Main
        //***************************

        $tiposUsuario = leer();
        if ($tiposUsuario->getId() != "" && $tiposUsuario->getNombre() != "") {
          $fichero = new Ficheros();
          $fichero->grabarTiposDeUsuario($tiposUsuario);
          echo "Grabado enlace. ";
          echo '<a href="TiposUsuariosMenu.php">Seguir</a>';
          //echo "Grabado: " . $enlace->getNombre() . "<br>";
        } else {
          //echo "Error: Campos vacios" . "<br>";
        }

        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
