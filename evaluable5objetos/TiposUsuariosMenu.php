<?php
include_once('Config.php');
include_once("funciones.php");
include_once('Ficheros.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php include_once("funciones.php"); ?>
        <?php include_once("Ficheros.php"); ?>
        <?php cabecera(); ?>

        <p>Gestión de Tipos de Usuarios:</p>
        <ul>
            <li><a href="TiposUsuariosFormulario.php">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id </td>';
        echo '<td>Nom </td>';
        echo '<td>Borrar</td>';
        echo '<td>Actualizar</td>';
        echo '</tr>';

        $datos = new Ficheros();
        $tiposUsuarios = $datos->getTiposdeUsuarios();

        if (count($tiposUsuarios) > 0) {

          foreach ($tiposUsuarios as $tiposUsuario) {
            echo "<tr>\n";
            echo "<td>" . $tiposUsuario->getId() . "</td>\n";
            echo "<td>" . $tiposUsuario->getNombre() . "</td>\n";
            $pos = array_search($tiposUsuario,$tiposUsuarios); //Guardamos la posición
            echo '<td> <a href="TiposUsuarioBorrar.php?id=' . $tiposUsuario->getId() . '">Borrar </td>';
            echo '<td> <a href="TiposUsuarioActualizar.php?pos=' . $pos . '">Actualizar </td>';
            echo "</tr>\n";
          }
        }

        echo "</table>";
        echo "<br/>";


        volver();
        pie();
        ?>

</html>
