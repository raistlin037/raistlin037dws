
<?php

class Config {

    static public $titulo = "Aplicación Web";
    static public $grupo = "Grupo3";
    static public $modulo = "DWS";
    static public $fecha = "29/11/2018";
    static public $empresa = "CEEDCV";
    static public $curso = "2018-19";
    static public $bdhostname = "localhost";
    static public $bdnombre = "ceedcv";
    static public $bdusuario = "alumno";
    static public $bdclave = "alumno";
    static public $modelo = "mysql";

    //Función que modifica la variable estática $modelo
    static public function setModelo($m) {
        switch ($m) {
            case 'fichero':
            self::$modelo = "fichero";
            break;
            case 'mysql':
            self::$modelo = "mysql";
            break;
            case 'postgres':
            self::$modelo = "postgres";            
            break;
        }
    }
}

?>
