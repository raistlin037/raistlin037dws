
<?php

// CONFIGURACION  CLEARDB HEROKU

class Config {

    static public $titulo = "Aplicación Web";
    static public $grupo = "Grupo3";
    static public $modulo = "DWS";
    static public $fecha = "29/11/2018";
    static public $empresa = "CEEDCV";
    static public $curso = "2018-19";
    static public $bdhostname = "eu-cdbr-west-02.cleardb.net";
    static public $bdnombre = "heroku_3aa993d01d0235f";
    static public $bdusuario = "bccc2ad3c9dac6";
    static public $bdclave = "2a861fd2";
    static public $modelo = "mysql";

    //Función que modifica la variable estática $modelo
    static public function setModelo($m) {
        switch ($m) {
            case 'fichero':
            self::$modelo = "fichero";
            break;
            case 'mysql':
            self::$modelo = "mysql";
            break;
            case 'postgres':
            self::$modelo = "postgres";            
            break;
        }
    }
}

?>
