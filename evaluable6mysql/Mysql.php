<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Enlaces.php");
include_once("TipoEnlace.php");
include_once("TiposDeUsuarios.php");
include_once("ConfigHeroku.php");

class Mysql {

    protected $conexion;

    public function __construct() {

        $this->conexion = NULL;

        try {

            $bdconexion = new PDO('mysql:host=' . Config::$bdhostname . ';dbname='
              . Config::$bdnombre . ';charset=utf8', Config::$bdusuario, Config::$bdclave);

            $this->conexion = $bdconexion;
        } catch (PDOException $e) {

            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getEnlaces1() {

        $enlaces = array();
        $filas = array();

        $consulta = "SELECT id, nombre, url, tipoenlace "
          . "FROM enlaces ORDER BY id";
        //echo $consulta;


        $filas = $this->conexion->query($consulta);

        $cont = 0;

        //print_r($filas);


        foreach ($filas as $fila) {
            //echo $fila->id . "<br>";
            $tipoenlace = new TipoEnlace("1", "Educacion");
            //$enlace = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
            $enlaces [$cont] = new Enlaces($fila['id'], $fila['nombre'], $fila['url'], $tipoenlace);
            //$enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url);
            $cont++;
        }

        $conexion = false;

        return $enlaces;
    }

    public function getEnlaces() {

        $enlaces = array();
        $consulta = "SELECT id, nombre, url, tipoenlace "
          . "FROM enlaces as a ORDER BY a.id";
        //echo $consulta;

        $result = $this->conexion->query($consulta);

        $cont = 0;
        //print_r($result);
        $filas = array();

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //$filas = $result->fetchAll();
        if (!$filas) {
            die("Execute query error, because: " . print_r($this->conexion->errorInfo(), true));
        } else {

            foreach ($filas as $fila) {
                //echo $fila->id . "<br>";
                $tipoenlace = new TipoEnlace("1", "Educacion");
                //$enlace = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
                $enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
                //$enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url);
                $cont++;
            }

            $conexion = false;
        }
        return $enlaces;
    }
    
    function grabarEnlace($enlace) {
        
        
        $consulta = 'INSERT INTO enlaces ( id, nombre, url, tipoenlace ) '
        . 'VALUES( :id,  :nombre, :url, :tipoenlace);';

        //echo $consulta . "<br\>";
        //print_r($enlace);
        echo "<br\>";
        echo "Grabando: " . $enlace->getId() . " " . $enlace->getNombre()
        . " " . $enlace->getUrl() . " " . $enlace->getTipoenlace()->getId();
        
        $result = $this->conexion->prepare($consulta);
        
        if ($result) {
            echo "<br>";
            //echo "\nPDO::errorInfo():<br>\n";
            //print_r($result);
            //print_r($this->conexion);
        }
        
        
        /* Ejecuta una sentencia preparada pasando un array de valores */
        $count = $result->execute(array(
            ":id" => $enlace->getId()
            , ":nombre" => $enlace->getNombre()
          , ":url" => $enlace->getUrl()
          , ":tipoenlace" => $enlace->getTipoenlace()->getId()
          )
        );
        
        //echo "Count: " . $count . "<br>";
        //print_r($result->errorInfo());
        
        $this->conexion = false;
        
        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    /***************** EMPIEZA EL CODIGO MODIFICADO POR EL ALUMNO *****************/
    
    public function getTiposdeUsuarios() {

        $tiposusuario = array();
        $consulta = "SELECT id, nombre "
          . "FROM tipousuario as a ORDER BY a.id";
        //echo $consulta;

        $result = $this->conexion->query($consulta);
        $cont = 0;
        //print_r($result);
        $filas = array();

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //$filas = $result->fetchAll();
        if (!$filas) {
            if ($result->rowCount()==0) {
                echo "No hay datos guardados";
            } else {
                die("Execute query error, because: " . print_r($this->conexion->errorInfo(), true));
            }
        } else {

            foreach ($filas as $fila) {
                //echo $fila->id . "<br>";
                //$tipoenlace = new TipoEnlace("1", "Educacion");
                //$enlace = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
                $tiposusuario [$cont] = new TiposDeUsuarios($fila->id, $fila->nombre);
                //$enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url);
                $cont++;
            }

            $conexion = false;
        }
        return $tiposusuario;
    }

    function actualizarTiposDeUsuario($id, $nombre, $pos) {
        try {
            $consulta = 'UPDATE tipousuario SET id=:id, nombre=:nombre WHERE id=:id;';

            echo "<br\>";
            echo "<p>Actualizando: ". $id." ".$nombre."</p>";

            $result = $this->conexion->prepare($consulta);

            $count = $result->execute(array(
                ":id" => $id, ":nombre" => $nombre)
            );
    
            if ($count == 1) {
                return true;
            } else {
                return false;
            }
            
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        } finally {
            $this->conexion = false;
        }
    }

    function grabarTiposDeUsuario($tiposUsuario) {
        
        $consulta = 'INSERT INTO tipousuario ( id, nombre) '
        . 'VALUES( :id,  :nombre);';
        
        //echo $consulta . "<br\>";
        //print_r($enlace);
        echo "<br\>";
        echo "<p>Grabando: " . $tiposUsuario->getId() . " " . $tiposUsuario->getNombre()."</p>";
        
        $result = $this->conexion->prepare($consulta);
        
        if ($result) {
            //echo "<br>";
            //echo "\nPDO::errorInfo():<br>\n";
            //print_r($result);
            //print_r($this->conexion);
        }
        
        
        /* Ejecuta una sentencia preparada pasando un array de valores */
        $count = $result->execute(array(
            ":id" => $tiposUsuario->getId()
            , ":nombre" => $tiposUsuario->getNombre()
            )
        );
        
        //echo "Count: " . $count . "<br>";
        //print_r($result->errorInfo());
        
        $this->conexion = false;
        
        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    function borrarTiposDeUsuario($id, $nombre) {

        try {
            $consulta = 'DELETE FROM tipousuario WHERE id = :id;';

            echo "<br\>";
            echo "<p>Eliminando: ".$id." ".$nombre."</p>";

            $result = $this->conexion->prepare($consulta);

            $count = $result->execute(array(
                ":id" => $id)
            );
    
            if ($count == 1) {
                return true;
            } else {
                return false;
            }
            
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        } finally {
            $this->conexion = false;
        }

    }

    function añadirAdmin() {
        $consulta = 'INSERT INTO usuarios ( id, nombre, tipousuario, usuario, password, email) '
        . 'VALUES( "1",  "Paco", "1", "admin", "admin", "paco@ceedcv.es");';
        $conexion = new PDO("mysql:host=" . Config::$bdhostname . ".", Config::$bdusuario, Config::$bdclave);
        $crear_bd = $conexion->prepare($consulta);
        $crear_bd->execute();
    }

    function instalar() {

        echo "<h2>Instalando: " . Config::$modelo . "</h2>";
        try {
// Conectamos sin indicar bbdd
            $conexion = new PDO("mysql:host=" . Config::$bdhostname . ".", Config::$bdusuario, Config::$bdclave);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

//creamos la base de datos si no existe
        try {
            $crear_bd = $conexion->prepare('CREATE DATABASE IF NOT EXISTS ' . Config::$bdnombre . ' COLLATE utf8_spanish_ci');
            $crear_bd->execute();
            echo "Creada BDDD: " . Config::$bdnombre . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

//decimos que queremos usar la BBDD que acabamos de crear
        try {
            $use_db = $conexion->prepare('USE ' . Config::$bdnombre);
            $use_db->execute();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        //creamos las tablas
        try {
            $sql = 'CREATE TABLE IF NOT EXISTS enlaces (
            id int(3) NOT NULL,
            nombre varchar(80) COLLATE utf8_spanish_ci,
            url varchar(80),
            tipoenlace int(3),
            PRIMARY KEY (id));';

            echo $sql . "<br>";

            $crear_tb_profesor = $conexion->prepare($sql);
            $crear_tb_profesor->execute();
            echo "Creada tabla: enlaces<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = 'CREATE TABLE IF NOT EXISTS tipoenlace (
            id int(11) NOT NULL ,
		    nombre varchar(100) COLLATE utf8_spanish_ci NOT NULL,
            PRIMARY KEY (id));';
            echo $sql . "<br>";
            $crear_tb_asignatura = $conexion->prepare($sql);
            $crear_tb_asignatura->execute();
            echo "Creada tabla: tipoenlace<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = 'CREATE TABLE IF NOT EXISTS usuarios (
            id int(3) NOT NULL,
            nombre varchar(80) COLLATE utf8_spanish_ci,
            tipousuario int(3),
            usuario varchar(80),
            password varchar(80),
            email varchar(80),
            PRIMARY KEY (id));';

            echo $sql . "<br>";

            $crear_tb_profesor = $conexion->prepare($sql);
            $crear_tb_profesor->execute();
            echo "Creada tabla: usuario<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = 'CREATE TABLE IF NOT EXISTS tipousuario (
            id int(11) NOT NULL ,
		    nombre varchar(100) COLLATE utf8_spanish_ci NOT NULL,
            PRIMARY KEY (id));';
            echo $sql . "<br>";
            $crear_tb_asignatura = $conexion->prepare($sql);
            $crear_tb_asignatura->execute();
            echo "Creada tabla: tipousuario<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = "ALTER TABLE enlaces ADD CONSTRAINT fk_enlace_tipoenlace FOREIGN KEY (tipoenlace) references tipoenlace(id);";
            $fk = $conexion->prepare($sql);
            $fk->execute();
            echo $sql . "<br>";
            echo "Creada CAj: Enlace.idp -> TipoEnlace.id <br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = "ALTER TABLE usuarios ADD CONSTRAINT fk_usuario_tipousuario FOREIGN KEY (tipousuario) references tipousuario(id);";
            $fk = $conexion->prepare($sql);
            $fk->execute();
            echo $sql . "<br>";
            echo "Creada CAj: Usuario.idp -> TipoUsuario.id <br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        $conexion = false;
    }

}

?>