<?php
include_once("ConfigHeroku.php");
include_once("funciones.php");
include_once("TiposDeUsuarios.php");
$modelo = recoge("modelo"); 
Config::setModelo($modelo);

switch (Config::$modelo) {
  case 'fichero':
  include_once('Ficheros.php');
  $datos = new Ficheros();
  break;
  case 'mysql':
  include_once('Mysql.php');
  $datos = new Mysql();
  break;
  case 'postgres':
  include_once('Postgres.php');
  $datos = new Postgres();
  break;
}
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php cabecera(); ?>

        <h3>TEMA 6. BASES DE DATOS. <?php echo strtoupper(Config:: $modelo) ?></h3>
        <h3>Gestión de Tipos de Usuarios:</h3>

        <p>
            <?php
                echo "<a href = '../index.php' >Inicio</a> > <a href = './index.php?modelo=$modelo' >Tema6</a> > <a href = './TiposUsuariosMenu.php?modelo=$modelo' >Tipos de usuarios</a> > Actualizar";
            ?>
        </p>

        <?php
            function leer() {

                $pos = recoge("pos");
                return $pos;
            }
            $pos = leer();
            
            $tiposUsuarios = $datos->getTiposdeUsuarios();
            $id = $tiposUsuarios[$pos]->getId();
            $nombre = $tiposUsuarios[$pos]->getNombre();
        ?>

        <h3>Actualizar Tipo Usuario</h3>

        <form action="TiposUsuariosActualizado.php" method="post">
            <table>
                <tr>
                    <td>Id</td>
                    <td><input type="text" name="id" value=<?php echo $id; ?> /></td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td><input type="text" name="nombre" value=<?php echo $nombre; ?> /></td>
                </tr>
            </table>

            <table>
                <tr>
                    <td>
                        <input type="submit" value="Modificar" />
                    </td>
                    <td>
                        <input type="reset" value="Borrar" />
                    </td>
                </tr>
            </table>
            <input type="hidden" name="pos" value=<?php echo $pos; ?> />
            <input type="hidden" name="modelo" value=<?php echo $modelo; ?> />
        </form>



        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>