<?php
include_once("ConfigHeroku.php");
include_once("funciones.php");
include_once("TiposDeUsuarios.php");
$modelo = recoge("modelo"); 
Config::setModelo($modelo); 

switch (Config::$modelo) {
  case 'fichero':
  include_once('Ficheros.php');
  $datos = new Ficheros();
  break;
  case 'mysql':
  include_once('Mysql.php');
  $datos = new Mysql();
  break;
  case 'postgres':
  include_once('Postgres.php');
  $datos = new Postgres();
  break;
}
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php cabecera(); ?>
        <h3>TEMA 6. BASES DE DATOS. <?php echo strtoupper(Config:: $modelo) ?></h3>
        <h3>Gestión de Tipos de Usuarios:</h3>
        
        <p>
          <?php
            echo "<a href = '../index.php' >Inicio</a> > <a href = './index.php?modelo=$modelo' >Tema6</a> > <a href = './TiposUsuariosMenu.php?modelo=$modelo' >Tipos de usuarios</a> > Alta";
          ?>
        </p>

        <?php

          function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");

            $tiposUsuario = new TiposDeUsuarios($id, $nombre);
            return $tiposUsuario;
          }

          //***************************
          //* Main
          //***************************

          $tiposUsuario = leer();
          if ($tiposUsuario->getId() != "" && $tiposUsuario->getNombre() != "") {

            $datos->grabarTiposDeUsuario($tiposUsuario);
            echo "Grabado tipo de usuario. ";
            echo "<br><br>";
            
          } else {
            echo "Error: Campos vacios" . "<br>";
          }
          
          echo "<a href='TiposUsuariosMenu.php?modelo=$modelo'>Seguir</a>";
          pie();
        ?>
    </body>
</html>
