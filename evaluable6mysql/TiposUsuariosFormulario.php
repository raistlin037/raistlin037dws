<?php
include_once('ConfigHeroku.php');
include_once("funciones.php");
$modelo = recoge("modelo"); //recogemos si se ha seleccionado fichero o mysql
Config::setModelo($modelo); //modificamos el valor de la constante $modelo
?>

<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        ?>

        <?php cabecera(); ?>
        <h3>TEMA 6. BASES DE DATOS. <?php echo strtoupper(Config:: $modelo) ?></h3>
        <h3>Alta Tipos de Usuarios</h3>

        <p>
            <?php
                echo "<a href = '../index.php' >Inicio</a> > <a href = './index.php?modelo=$modelo' >Tema6</a> > <a href = './TiposUsuariosMenu.php?modelo=$modelo' >Tipos de usuarios</a> > Actualizar";
            ?>
        </p>

        <form action="TiposUsuariosAlta.php" method="post">
            <table>
                <tr>
                    <td>Id</td>
                    <td><input type="text" name="id" value="1"/> </td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td><input type="text" name="nombre" value="Profesor"/></td>
                </tr>
                
            </table>

            <table>
                <tr>
                    <td>
                        <input type="submit" value="Enviar" />
                    </td>
                    <td>
                        <input type="reset" value="Borrar" />
                    </td>
                </tr>
            </table>
            <input type="hidden" name="modelo" value=<?php echo $modelo; ?>>
        </form>



        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>