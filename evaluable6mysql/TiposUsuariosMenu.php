<?php
include_once('ConfigHeroku.php');
include_once("funciones.php");
include_once("TiposDeUsuarios.php");
$modelo = recoge("modelo"); //recogemos si se ha seleccionado fichero o mysql
Config::setModelo($modelo); //modificamos el valor de la constante $modelo

switch (Config::$modelo) {
    case 'fichero':
        include_once('Ficheros.php');
        $datos = new Ficheros();
        break;
    case 'mysql':
        include_once('Mysql.php');
        $datos = new Mysql();
        break;
    case 'postgres':
        include_once('Postgres.php');
        $datos = new Postgres();
        break;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php cabecera(); ?>
        <h3>TEMA 6. BASES DE DATOS. <?php echo strtoupper(Config:: $modelo) ?></h3>
        <h3>Gestión de Tipos de Usuarios:</h3>
        <p>
            <?php
                echo "<a href = '../index.php' >Inicio</a> > <a href = './index.php?modelo=$modelo' >Tema6</a> > Tipos de usuarios";
            ?>
        </p>
        <ul>
            <?php
                echo "<li><a href='TiposUsuariosFormulario.php?modelo=$modelo'>Alta </a> </li>";
            ?>
        </ul>
        <?php

        $tiposUsuarios = $datos->getTiposdeUsuarios();

        if (count($tiposUsuarios) > 0) {
            
            echo "<p>Listado:</p>";
            echo '<table border="1" with="100">';
            echo '<tr>';
            echo '<td>Id </td>';
            echo '<td>Nom </td>';
            echo '<td>Borrar</td>';
            echo '<td>Actualizar</td>';
            echo '</tr>';

          foreach ($tiposUsuarios as $tiposUsuario) {
            echo "<tr>\n";
            echo "<td>" . $tiposUsuario->getId() . "</td>\n";
            echo "<td>" . $tiposUsuario->getNombre() . "</td>\n";
            $pos = array_search($tiposUsuario,$tiposUsuarios); //Guardamos la posición
            echo '<td> <a href="TiposUsuarioBorrar.php?id='.$tiposUsuario->getId().'&nombre='.$tiposUsuario->getNombre().'&modelo='.$modelo.'">Borrar </td>';
            echo '<td> <a href="TiposUsuarioActualizar.php?pos=' . $pos .'&modelo='.$modelo.'">Actualizar </td>';
            echo "</tr>\n";
          }
        }

        echo "</table>";
        echo "<br/>";


        volver();
        pie();
        ?>

</html>
