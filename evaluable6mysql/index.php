<?php 
include_once("funciones.php");
include_once("ConfigHeroku.php"); 
$modelo = recoge("modelo"); //recogemos si se ha seleccionado fichero o mysql
Config::setModelo($modelo); //modificamos el valor de la constante $modelo
?>

<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>
        <h3>TEMA 6. BASES DE DATOS. <?php echo strtoupper(Config::$modelo) ?></h3>
        
        <p>
            <a href = "../index.php" >Inicio</a> > Tema6
        </p>

        <p>Elegir:</p>
        <ul>
            <li><a href="EnlacesMenu.php">Profesor. Paco Aldarias. Gestión de enlaces</a> </li>
            <li><a href="TiposEnlaces.php">Alumno1. Gestión de tipos de enlaces </a> </li>
            <li><a href="UsuariosMenu.php">Alumno2. Gestión de usuarios</a> </li>
            <?php
                echo "<li><a href='TiposUsuariosMenu.php?modelo=$modelo'>Alumno3. Gestión de tipos de usuarios</a> </li>";
            ?>
        </ul>

        <?php
            if(Config::$modelo == "mysql") {
                echo "<p>Mysql Elegir:</p>";
                echo "<ul><li><a href='instalarbd.php'>Instalar BBDD</a></li></ul>";
            }
        ?>

        <p>Documentación por tema:</p>
        <ul><li><a href="https://docs.google.com/document/d/1lZqZ77U8Yk6aNdtvKn-mh__fQWIdNdT3m52IbUQNYeY/edit?usp=sharing"  target="docu6">Doc</a> </li>
        </ul>

        <?php pie(); ?>

    </body>
</html>