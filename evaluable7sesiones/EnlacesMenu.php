<?php
session_start();
if ($_SESSION['SesionValida'] == 0) {
    header("Location: sesiones.php");
}
include_once('Config.php');
include_once("funciones.php");
include_once("Enlaces.php");
include_once("TipoEnlace.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>

        <h2>Tema6. BBDD. Paco Aldarias</h2>

        <h3>Gestión de Enlaces:</h3>
        <?php echo "<h4>Fuente de datos: " . $_SESSION['datos'] . '</h4>'; ?>

        <p>
            Inicio > <a href = "./index.php" >  Tema 7  </a> > Enlaces
        </p>

        <ul>
            <li><a href="EnlaceFormulario.php">Alta </a> </li>

        </ul>

        <?php
        switch ($_SESSION['datos']) {
            case 'ficheros':
                include_once("Ficheros.php");
                $datos = new Ficheros();
                break;
            case 'mysql':
                include_once("Mysql.php");
                $datos = new Mysql();
                break;
            case 'postgres':
                include_once("Postgres.php");
                $datos = new Postgres();
                break;
            case 'sqlite':
                include_once("Sqlite.php");
                $datos = new Sqlite();
                break;
        }

        $enlaces = array();
        $enlaces = $datos->getEnlaces();

        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id </td>';
        echo '<td>Nom </td>';
        echo '<td>Url </td>';
        echo '<td>TipoUrl</td>';
        echo '<td>Borrar</td>';
        echo '<td>Actualizar</td>';
        echo '</tr>';

        foreach ($enlaces as $enlace) {
            //print_r($enlace);
            echo "<tr>\n";
            echo "<td>" . $enlace->getId() . "</td>\n";
            echo "<td>" . $enlace->getNombre() . "</td>\n";
            echo "<td>" . $enlace->getUrl() . "</td>\n";
            echo "<td>" . $enlace->getTipoenlace()->getId() . "</td>\n";
            echo '<td> <a href="EnlacesBorrar.php?id=' . $enlace->getId() . '">Borrar </td>';
            echo '<td> <a href="EnlacesActualizar.php?id=' . $enlace->getId() . '">Actualizar </td>';
            echo "</tr>\n";
        }
        echo "</table>";

        echo "<br/>";

        pie();
        ?>

</html>
