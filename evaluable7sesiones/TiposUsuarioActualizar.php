<?php
session_start();
if ($_SESSION['SesionValida'] == 0) {
    header("Location: sesiones.php");
}
include_once('ConfigHeroku.php');
include_once("funciones.php");
include_once("TiposDeUsuarios.php");

switch ($_SESSION['datos']) {
    case 'ficheros':
        include_once("Ficheros.php");
        $datos = new Ficheros();
        break;
    case 'mysql':
        include_once("Mysql.php");
        $datos = new Mysql();
        break;
    case 'postgres':
        include_once("Postgres.php");
        $datos = new Postgres();
        break;
    case 'sqlite':
        include_once("Sqlite.php");
        $datos = new Sqlite();
        break;
}
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php cabecera(); ?>

        <h3>TEMA 7. SESIONES. <span style="color:blue"><?php echo strtoupper($_SESSION['datos']) ?></span></h3>
        <h3>Gestión de Tipos de Usuarios:</h3>

        <p>
            <a href = "../index.php" >Inicio</a> > <a href = "./index.php" >Tema7</a> > Tipos de usuarios > Actualizar
        </p>

        <?php
            function leer() {

                $pos = recoge("pos");
                return $pos;
            }
            $pos = leer();
            
            $tiposUsuarios = $datos->getTiposdeUsuarios();
            $id = $tiposUsuarios[$pos]->getId();
            $nombre = $tiposUsuarios[$pos]->getNombre();
        ?>

        <h3>Actualizar Tipo Usuario</h3>

        <form action="TiposUsuariosActualizado.php" method="post">
            <table>
                <tr>
                    <td>Id</td>
                    <td><input type="text" name="id" value=<?php echo $id; ?> /></td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td><input type="text" name="nombre" value=<?php echo $nombre; ?> /></td>
                </tr>
            </table>

            <table>
                <tr>
                    <td>
                        <input type="submit" value="Modificar" />
                    </td>
                    <td>
                        <input type="reset" value="Borrar" />
                    </td>
                </tr>
            </table>
            <input type="hidden" name="pos" value=<?php echo $pos; ?> />
        </form>



        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>