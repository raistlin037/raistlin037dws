<?php
session_start();
if ($_SESSION['SesionValida'] == 0) {
    header("Location: sesiones.php");
}
include_once('ConfigHeroku.php');
include_once("funciones.php");
include_once("TiposDeUsuarios.php");

switch ($_SESSION['datos']) {
    case 'ficheros':
        include_once("Ficheros.php");
        $datos = new Ficheros();
        break;
    case 'mysql':
        include_once("Mysql.php");
        $datos = new Mysql();
        break;
    case 'postgres':
        include_once("Postgres.php");
        $datos = new Postgres();
        break;
    case 'sqlite':
        include_once("Sqlite.php");
        $datos = new Sqlite();
        break;
}

error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php cabecera(); ?>
        <h3>TEMA 7. SESIONES. <span style="color:blue"><?php echo strtoupper($_SESSION['datos']) ?></span></h3>
        <h3>Gestión de Tipos de Usuarios:</h3>

        <p>
            <a href = "../index.php" >Inicio</a> > <a href = "./index.php" >Tema7</a> > Tipos de usuarios > Borrar
        </p>

        <?php

            $id = recoge("id");
            $nombre = recoge("nombre");

            if ($id != "" && $nombre != "") {
                $datos->borrarTiposDeUsuario($id, $nombre);
                echo "Borrado tipo de usuario. ";
                echo '<a href="TiposUsuariosMenu.php">Seguir</a>';
                //echo "Grabado: " . $enlace->getNombre() . "<br>";
            } else {
                echo "Error: Campos vacios" . "<br>";

            }

            //header('Location: EnlacesMenu.php');
            pie();
        ?>
    </body>
</html>
