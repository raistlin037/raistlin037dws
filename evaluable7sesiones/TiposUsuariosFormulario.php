<?php
session_start();
if ($_SESSION['SesionValida'] == 0) {
    header("Location: sesiones.php");
}
include_once('ConfigHeroku.php');
include_once("funciones.php");
?>

<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        ?>

        <?php cabecera(); ?>
        <h3>TEMA 7. SESIONES. <span style="color:blue"><?php echo strtoupper($_SESSION['datos']) ?></span></h3>
        <h3>Alta Tipos de Usuarios</h3>

        <p>
            <a href = "../index.php" >Inicio</a> > <a href = "./index.php" >Tema7</a> > <a href = "./TiposUsuariosMenu.php" >Tipos de usuarios</a> > Actualizar
        </p>

        <form action="TiposUsuariosAlta.php" method="post">
            <table>
                <tr>
                    <td>Id</td>
                    <td><input type="text" name="id" value="1"/> </td>
                </tr>
                <tr>
                    <td>Nombre</td>
                    <td><input type="text" name="nombre" value="Profesor"/></td>
                </tr>
                
            </table>

            <table>
                <tr>
                    <td>
                        <input type="submit" value="Enviar" />
                    </td>
                    <td>
                        <input type="reset" value="Borrar" />
                    </td>
                </tr>
            </table>
        </form>



        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>