<?php
session_start();
if ($_SESSION['SesionValida'] == 0) {
    header("Location: sesiones.php");
}
include_once("funciones.php");
include_once("TiposDeUsuarios.php");

switch ($_SESSION['datos']) {
    case 'ficheros':
        include_once("Ficheros.php");
        $datos = new Ficheros();
        break;
    case 'mysql':
        include_once("Mysql.php");
        $datos = new Mysql();
        break;
    case 'postgres':
        include_once("Postgres.php");
        $datos = new Postgres();
        break;
    case 'sqlite':
        include_once("Sqlite.php");
        $datos = new Sqlite();
        break;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php cabecera(); ?>
        <h3>TEMA 7. SESIONES. <span style="color:blue"><?php echo strtoupper($_SESSION['datos']) ?></span></h3>
        <h3>Gestión de Tipos de Usuarios:</h3>
        <p>
            <a href = "../index.php" >Inicio</a> > <a href = "./index.php" >Tema7</a> > Tipos de usuarios
        </p>
        <ul>
            <li><a href="TiposUsuariosFormulario.php">Alta </a> </li>
        </ul>
        <?php

        $tiposUsuarios = $datos->getTiposdeUsuarios();

        if (count($tiposUsuarios) > 0) {
            
            echo "<p>Listado:</p>";
            echo '<table border="1" with="100">';
            echo '<tr>';
            echo '<td>Id </td>';
            echo '<td>Nom </td>';
            echo '<td>Borrar</td>';
            echo '<td>Actualizar</td>';
            echo '</tr>';

          foreach ($tiposUsuarios as $tiposUsuario) {
            echo "<tr>\n";
            echo "<td>" . $tiposUsuario->getId() . "</td>\n";
            echo "<td>" . $tiposUsuario->getNombre() . "</td>\n";
            $pos = array_search($tiposUsuario,$tiposUsuarios); //Guardamos la posición
            echo '<td> <a href="TiposUsuarioBorrar.php?id='.$tiposUsuario->getId().'&nombre='.$tiposUsuario->getNombre().'">Borrar </td>';
            echo '<td> <a href="TiposUsuarioActualizar.php?pos=' . $pos . '">Actualizar </td>';
            echo "</tr>\n";
          }
        }

        echo "</table>";
        echo "<br/>";


        volver();
        pie();
        ?>

</html>
