-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 04-12-2018 a las 13:01:37
-- Versión del servidor: 5.7.24-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ceedcv`
--
-- --------------------------------------------------------
CREATE DATABASE IF NOT EXISTS `ceedcv` COLLATE utf8_spanish_ci;

--
-- Estructura de tabla para la tabla `enlaces`
--
CREATE TABLE IF NOT EXISTS `enlaces` (
  `id` int(3) NOT NULL,
  `nombre` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `url` varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipoenlace` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Estructura de tabla para la tabla `tipoenlace`
--
CREATE TABLE IF NOT EXISTS `tipoenlace` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Estructura de tabla para la tabla `usuarios`
--
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipousuario` int(3) DEFAULT NULL,
  `usuario` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Estructura de tabla para la tabla `tipousuario`
--
CREATE TABLE IF NOT EXISTS `tipousuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Indices de la tabla `tipoenlace`
--
ALTER TABLE `tipoenlace`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `enlaces`
--
ALTER TABLE `enlaces`
  ADD PRIMARY KEY (`id`),
  ADD FOREIGN KEY (`tipoenlace`) references `tipoenlace`(`id`);

--
-- Indices de la tabla `tipousuario`
--
ALTER TABLE `tipousuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD FOREIGN KEY (`tipousuario`) references `tipousuario`(`id`);

--
-- Volcado de datos para la tabla `tipoenlace`
--
INSERT INTO `tipoenlace` (`id`, `nombre`) VALUES
(1, 'Educación');

--
-- Volcado de datos para la tabla `enlaces`
--
INSERT INTO `enlaces` (`id`, `nombre`, `url`, `tipoenlace`) VALUES
(1, 'Paco', 'http://www.ceedcv.es', 1),
(2, 'CEED', 'http://www.ceedcv.es', 1);

--
-- Volcado de datos para la tabla `tipousuario`
--
INSERT INTO `tipousuario` (`id`, `nombre`) VALUES
(1, 'profesor'),
(2, 'alumno');

--
-- Volcado de datos para la tabla `usuarios`
--
INSERT INTO `usuarios` (`id`, `nombre`, `tipousuario`, `usuario`, `password`, `email`) VALUES
(1, 'Paco', 1, 'admin', 'admin', 'paco.aldarias@ceedcv.es'),
(2, 'Javi', 2, 'alumno', 'alumno', 'Raistlin037@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
