<?php
session_start();
if ($_SESSION['SesionValida'] == 0) {
    header("Location: sesiones.php");
}
?>

<?php include_once("funciones.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>
        <h3>TEMA 7. SESIONES. <?php echo strtoupper($_SESSION['datos']) ?></h3>
        <h4><a href="logout.php">Cerrar Sesión</a></h4>

        <p>
            <a href = "../index.php" >Inicio</a> >Tema7
            <br>
        </p>

        <p>Elegir:</p>
        <ul>
            <li><a href="EnlacesMenu.php">Profesor. Paco Aldarias. Gestión de enlaces</a> </li>
            <li><a href="TiposEnlaces.php">Alumno1. Gestión de tipos de enlaces </a> </li>
            <li><a href="UsuariosMenu.php">Alumno2. Gestión de usuarios</a> </li>
            <li><a href="TiposUsuariosMenu.php">Alumno3. Gestión de tipos de usuarios</a> </li>
        </ul>
        
        <?php
            if($_SESSION['datos'] == "mysql" || $_SESSION['datos'] == "postgres" || $_SESSION['datos'] == "sqlite") {
                echo "<p>Mysql Elegir:</p>";
                echo "<ul><li><a href='instalarbd.php'>Instalar BBDD</a></li></ul>";
            }
        ?>

        <p>Documentación por tema:</p>
        <ul><li><a href="https://docs.google.com/document/d/1lZqZ77U8Yk6aNdtvKn-mh__fQWIdNdT3m52IbUQNYeY/edit?usp=sharing"  target="docu6">Doc</a> </li>
        </ul>

        <?php pie(); ?>

    </body>
</html>
