<?php
session_start();
?>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <?php
        include_once('ConfigHeroku.php');
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        include_once("Mysql.php");
        ?>

        <h2>Control de Acceso</h2>
        <h2>Tema 7. Sesiones</h2>
        <form method="POST" action="" >
            <table border="1">

                <tr>
                    <td>Usuario</td>
                    <td>
                        <input type="text" name="usuario" value="admin" default="admin">
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" name="password" value="admin" default="admin">
                    </td>
                </tr>
                <tr>
                    <td>Fuente de datos</td>
                    <td>

                        <?php
                        echo '<select name="datos">';
                        $fuentes = array('ficheros', 'mysql');
                        $string = "";
                        foreach ($fuentes as $i => $value) {
                            //<option selected>
                            $string = '<option ';

                            // Dejamos marcada la opcion elegida

                            if (isset($_REQUEST["datos"])) {
                                if ($value == $_REQUEST["datos"]) {
                                    $string .= ' selected';
                                }
                            }

                            $string .= '>';
                            $string .= $value;
                            $string .= '</option>';
                            echo $string;
                        }
                        echo '</select>';
                        ?>

                    </td>
                </tr>
                <tr>
                    <td><input type="submit"  name= "Enviar" value="Enviar"></td>
                    <td><input type="reset"   name= "Borrar" value="Borrar"></td>
                </tr>
            </table>
        </select>
    </form>

    <p>Documentación evaluable 7 Sesiones:</p>
    <ul><li><a href="https://docs.google.com/document/d/1_igu8QZqYmMzuHn_NjaS3jZVm6Z0pKYZmJ9bDCviBrI/edit?usp=sharing"  target="docu6">Doc</a> </li>
    </ul>

    <?php
    if (isset($_REQUEST["datos"])) {
        $_SESSION['datos'] = $_REQUEST["datos"];
        switch ($_SESSION['datos']) {
            case 'ficheros':
            include_once("Ficheros.php");
            $datos = new Ficheros();
            break;
            case 'mysql':
            $datos = new Mysql();
            break;
            case 'postgres':
            include_once("Postgres.php");
            $datos = new Postgres();
            break;
            case 'sqlite':
            include_once("Sqlite.php");
            $datos = new Sqlite();
            break;
        }
    }

    
    //Consultamos el usuario y la contraseña en base de datos
    if (isset($_REQUEST["datos"])) {
        $bd = new Mysql();
        if (isset($_REQUEST["usuario"]) && isset($_REQUEST["password"])) {
            if ($bd->comprobarUsuario($_REQUEST["usuario"])) {
                if ($bd->comprobarPassword($_REQUEST["password"])) {
                    $_SESSION['SesionValida'] = 1;
                    echo 'Correcto. <a href=" ./index.php">Seguir</a>';
                } else {
                    echo "Error: Password no encontrado<br>";
                }
            } else {
                echo "Error: Usuario no encontrado<br>";
            }
        }
    }
    ?>
