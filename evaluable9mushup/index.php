<?php 
    include_once("funciones.php"); 
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="trivial.css">
    </head>
    <body>

        <?php cabecera(); ?>
        <h3>TEMA 9. MASHUP</h3>

        <form method="post" action="<?=$_SERVER ['PHP_SELF']?>">
        <ol>
        <?php
            if (isset($_POST["preguntas"])) {
                $questions = unserialize(base64_decode($_POST["preguntas"]));
            } else {
                $url='https://opentdb.com/api.php?amount=10&difficulty=easy';
                $json = file_get_contents($url);
                $questions = json_decode($json,TRUE)['results'];
            }
    
            foreach($questions as $clave=>$question) {
                echo "<li class='categoria'>".$question['category']."</li>";
                echo "<p class='pregunta'>".$question['question']."</p>";
                $answers = $question['incorrect_answers'];
                array_push($answers, $question['correct_answer']);
                shuffle($answers);
                foreach($answers as $answer) {
                    if(isset($_POST["preguntas"])) {
                        if(isset($_POST[(string)$clave])) {
                            if($question['correct_answer'] == $answer && $_POST[(string)$clave] == $answer) {
                                echo "<input type='radio' value='".$answer."' name='".(string)$clave."' checked><span class='correcta'>".$answer."</span><br>";
                            } else if($question['correct_answer'] != $_POST[(string)$clave] && $_POST[(string)$clave] == $answer) {
                                echo "<input type='radio' value='".$answer."' name='".(string)$clave."' checked><span class='error'>".$answer."</span><br>";
                            } else if($question['correct_answer'] == $answer) {
                                echo "<input type='radio' value='".$answer."' name='".(string)$clave."'><span class='correcta'>".$answer."</span><br>";
                            } else {
                                echo "<input type='radio' value='".$answer."' name='".(string)$clave."'>".$answer."<br>";
                            }
                        } else if($question['correct_answer'] == $answer) {
                            echo "<input type='radio' value='".$answer."' name='".(string)$clave."'><span class='correcta'>".$answer."</span><br>";
                        } else {
                            echo "<input type='radio' value='".$answer."' name='".(string)$clave."'>".$answer."<br>";
                        }
                    } else {
                        echo "<input type='radio' value='".$answer."' name='".(string)$clave."'>".$answer."<br>";
                    }
                }
            }

            ?>
        </ol>
        <input type="hidden" name="preguntas" value=<?php echo base64_encode(serialize($questions)); ?>>
        <input class="boton" type="submit" value="COMPROBAR">
        <a href="index.php">VOLVER A JUGAR</a>
        </form>

        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>