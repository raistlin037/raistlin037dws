<!DOCTYPE html>
<html>
    <head>
        <title>DAW2 DWS.</title>
        <meta charset="UTF-8">
    </head>
    <body>

        <h1>APLICACIÓN WEB</h1>
        <h2>DWS. GRUPO 3</h2>

        <hr/>

        <p>Inicio</p>

        <p>Elegir:</p>
        <ul>
            <li><a href="evaluable2php/index.php">Tema 2. Php</a> </li>
            <li><a href="evaluable3formularios/index.php">Tema 3. Formularios</a> </li>
            <li><a href="evaluable4ficheros/index.php">Tema 4. Ficheros</a> </li>
            <li><a href="evaluable5objetos/index.php">Tema 5. Objetos</a> </li>
            <li>Tema 6. BBDD
                <select name="modelo" id="modelo">
                    <option selected>Elegir modelo</option>
                    <option value="evaluable6mysql/index.php?modelo=mysql">Mysql</option>
                    <option value="evaluable6mysql/index.php?modelo=fichero">Fichero</option>
                </select> </li>
            <li><a href="evaluable7sesiones/sesiones.php">Tema 7. Sesiones</a> </li>
            <li><a href="evaluable9mushup/index.php">Tema 9. Mashup</a> </li>
        </ul>

        <script>
            document.getElementById("modelo").onchange = function() {
                if (this.selectedIndex!==0) {
                    window.location.href = this.value;
                }        
            };
        </script>

        <p>Datos del grupo:</p>

        <table border='1'>
            <tr>
                <th>Alumno#</th>
                <th>Nombre</th>
                <th>Bitbucket</th>
                <th>Heroku</th>
            </tr>

            <tr>
                <td>Profesor</td>
                <td>Paco Aldarias</td>

                <td>
                    <a href="https://bitbucket.org/pacoaldarias/pacoaldariasdws" target="tema2p">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://pacoaldariasdws.herokuapp.com" target="heroku">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno1</td>
                <td>Jose Ortega</td> 
                <td>
                    <a href="https://bitbucket.org/joorce/joorcedwst2e1" target="tema2b1">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://joorcedws.herokuapp.com" target="tema2h1">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno2</td>
                <td>David Barberá Zahonero</td>
                <td>
                    <a href="https://bitbucket.org/dabaza/davidbarberadws" target="tema2b2">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://davidbarberadws.herokuapp.com/" target="tema2h2">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno3</td>
                <td>Javier Alcolea Ruiz</td>
                <td>
                    <a href="https://bitbucket.org/raistlin037/raistlin037dws" target="tema2b3">Bitbucket</a>
                </td>

                <td>
                    <a href ="https://raistlin037dws.herokuapp.com" target="tema2h3">Heroku</a>
                </td>


            </tr>
        </table>




        <ul>
            <li>
                <a href ="https://docs.google.com/document/d/1hq9DX2v-6RIIWdquZv9Kv0ui6KqI9ETPX8M9os4U820/edit?ts=5bbce89c" target="docgru">Documentación del grupo</a>
            </li>
        </ul>

    </p>


    <hr/><pre> CEEDCV  DAW2 GRUPO3 </pre>
    
</body>
</html>

